# Advanced Style Framework

## General

The [Nimiq CSS framework](https://github.com/nimiq/nimiq-style), better.

Please visit the [demo page](https://felix-schindler.gitlab.io/advanced-style/) for a simple guide on how it looks, how to use and what elements there are.

For a real-word example please visit [mc.schindlerfelix.de](https://mc.schindlerfelix.de).

Featuring:
- Automaric dark mode
- Even more styled HTML elements
- Not having to use so many classes, for cleaner and simpler HTML

## Build

```bash
yarn build
```

## 💭 Feature ideas for the far future

- [ ] Add classes back in so you can use them if you want to
- [ ] Icon font
- [ ] Different [standard layouts](https://web.dev/one-line-layouts/)
    - Header, Main, Footer
    - Header, Main | Aside, Footer
    - Header, Left | Main | Right, Footer
    - Header, Left | Main, Footer
